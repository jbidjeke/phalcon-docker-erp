# phalcon-docker-erp

Api Erp Phalcon with Docker platform 

## Getting started

To make it easy for you to get started with Erp project, here's a list of recommended next steps.

I transferred to gitlab.com, on the master branch, the docker infrastructure and the erp app in the phalcon folder.
here is the link => https://gitlab.com/jbidjeke/phalcon-docker-erp/-/tree/master

This will facilitate the launch of the application without too much configuration.
it will simply suffice to:

- [ ] Download to a folder with the command 
```
git clone https://gitlab.com/jbidjeke/phalcon-docker-erp.git
```

- [ ] Go to platform directory
```
cd phalcon-docker-erp
```

- [ ] Run docker compose command
```
docker-compose up -d
```

- [ ] Command to enter in container
```
docker exec -it php sh
```

- [ ] Run php unit
```
vendor/bin/phpunit
```

- [ ] Use host  
```
http://localhost:8080/
```

- [ ] Load Pre-configured postman file to ckecking Api 
```
./postman/kinaxia-erp.postman_collection.json
```
